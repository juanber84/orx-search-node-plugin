'use strict'

const findConfig = require('find-config')
const orxsearchConfigPath = findConfig('orxsearch.js')

let config = require(orxsearchConfigPath)

var elasticsearch = require('elasticsearch')

var client = new elasticsearch.Client(config.credentials)


client.indices.create({
    index: 'orx'
}, function (err, resp, status) {
    if (err) {
        // console.log(err);
    } else {
        console.log("create", resp)
    }
})

// client.indices.create({
//     index: 'orx',
//     // mapping: {
//     //     house: {
//     //         name: {
//     //             type: 'string'
//     //         }
//     //     }
//     // }
// }, function (err) {
//     console.log(err)    
// })

module.exports = client