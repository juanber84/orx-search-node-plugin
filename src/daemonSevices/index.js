#!/usr/bin/env node

'use strict'

const findConfig = require('find-config')
const orxsearchConfigPath = findConfig('orxsearch.js')

let config = require(orxsearchConfigPath)

const logPath = config.log + '/orx-search-log'
var fs = require('fs')

var filenames = fs.readdirSync(logPath).map(function (file) { return logPath + '/' + file })

const Tail = require('tail').Tail;

let operations = []

let esCLient = require('./../fn/esClient')

filenames.forEach(function (f) {
    if (f.includes(".audit.json"))
        return
    let tail = new Tail(f)
    tail.on("line", function (data) {
        // Parse
        try {
            data = JSON.parse(data)
            // Get event
            let event = data._event
            // Remove invalida data
            delete data.message
            delete data.level
            delete data._event
            // Cache data
            if (event === 'create') {
                operations.push({
                    index: {
                        _index: 'orx',
                        _type: data.namespace,
                        _id: data.id
                    }
                }, data)
            }
            if (event === 'destroy') {
                operations.push({
                    delete: {
                        _index: 'orx',
                        _type: data.namespace,
                        _id: data.id
                    }
                })
            }
            if (event === 'update') {
                operations.push({
                    update: {
                        _index: 'orx',
                        _type: data.namespace,
                        _id: data.id
                    }
                }, data)
            }
        } catch (err) {
            console.log('Invalid data')
        }

    })

    tail.on("error", function (data) {
    })
})

setInterval(function () {
    // if (operations.length > 10) {
        let insertOperations = operations
        operations = []
        if (operations.length > 0)
            esCLient.bulk({ body: insertOperations })
    // }
}, config.time)