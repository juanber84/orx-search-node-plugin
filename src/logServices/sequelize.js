'use strict'

const findConfig = require('find-config')
const _ = require('lodash')
const winston = require('winston')
require('winston-logrotate')
const mkdirp = require('mkdirp')
const co = require('co')

let events = [
    {
        event: 'afterCreate',
        name: 'create'
    },
    {
        event: 'afterDestroy',
        name: 'destroy'
    },
    {
        event: 'afterUpdate',
        name: 'update'
    },
    // TODO posibles new events
    // {
    //     event: 'afterSave',
    //     name: 'create'
    // },
    // {
    //     event: 'afterUpsert',
    //     name: 'update'
    // }       
]

const orxsearchConfigPath = findConfig('orxsearch.js')

let config = require(orxsearchConfigPath)

function filterData(dataValues, config) {
    let record = {}
    for (var key in config.data) {
        if (_.isArray(_.get(config, 'data.' + key))) {
            let temporalDescription = ''
            config.data[key].forEach(k => {
                temporalDescription = temporalDescription + _.get(dataValues, k) + ' '
            })
            record[key] = temporalDescription
        } else {
            record[key] = _.get(dataValues, _.get(config, 'data.' + key))
        }
    }
    record.namespace = config.namespace
    return record
}

let logger

const logPath = config.log + '/orx-search-log'

module.exports = function (sequelize) {
    co(function* () {
        // Load log
        yield new Promise(function (resolve, reject) {
            mkdirp(logPath, function (err) {
                if (err)
                    reject(err)
                else {
                    var rotateTransport = new winston.transports.Rotate({
                        file: logPath + '/application.log',
                        colorize: false,
                        timestamp: false,
                        json: false, 
                        size: '20m',
                        keep: 5,
                        compress: false,
                        formatter: function (options) {
                            return JSON.stringify(options.meta)
                        }
                    })
                    logger = new (winston.Logger)({
                        transports: [
                            rotateTransport
                        ]
                    })
                    resolve()
                }
            })
        })
        // Load events
        events.forEach(e => {
            sequelize.addHook(e.event, (instance) => {
                // Get model data to save
                let configurationToSave = _.get(config, 'transformations.' + instance.constructor.name)
                // Validate if models exists
                if (configurationToSave && configurationToSave.data) {
                    let record = filterData(instance.dataValues, _.get(config, 'transformations.' + instance.constructor.name))
                    if (record) {
                        record._event = e.name
                        logger.info(record)
                    }
                }
            })
        })
    })
    .catch(err => {
        console.log(err)
    }) 

}