# Installation
```
const sequelizeClient = require('./sequelizeClient')

// Modify client
const orxSearchNodePlugin = require('@gigigo/orx-search-node-plugin').sequelize
orxSearchNodePlugin(sequelizeClient)
```

# Configuration
```
$ touch orxsearch.js
```

```
module.exports = {
    transformations: {
        user: {
            data: {
                id: 'id',
                projectId: 'projectId',
                description: [
                    'firstName',
                    'lastName'
                ],
            },
            namespace: 'pt.user'
        },

        data: {
            data: {
                id: 'id',
                projectId: 'projectId',
                description: 'data1'
            },
            namespace: 'pt.data'
        }
    },
    credentials: {
        "host": "127.0.0.1:9200",
        "httpAuth": "elastic:changeme"
    },
    log: __dirname,
    time: 10000
}

```